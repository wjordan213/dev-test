# PolicyMic Development Test

## General Guidelines
This is a Full Stack (PHP, JS, HTML, SCSS) hiring competency test designed to gauge skill, code completeness and accuracy, and affinity for standards based development.

1. Feel free to email <anthony@policymic.com> with any questions you may have.
1. The code that you write should follow best practices for PHP, HTML(5), SCSS, and JavaScript.
1. You may use any libraries that you choose for this task.  A local copy of jQuery has been included.
1. All HTML should be placed in index.php.  All other resources should be saved where appropriate.

### Submission
The preferred method is delivery via bitbucket. Fork this repository, complete the test, then send a pull request. If you require an alternate delivery method please email <anthony@policymic.com>.
### Anything else?
You should commit your progress often and write clear and informative commit messages.

Feel free to show off.

## PHP Guidelines
The included index.php file loads `data/articles.php` that contains an array stored in a global variable named `$articles`.  Iterate through this array using a custom Article PHP class `(lib/Article.php)` that does the following:

1. Generates a full name from the first and last name keys
1. Converts the date by using a static php method from the format `2013-03-17 21:50:04` to March 17th, 2013.
1. Initially orders the data set by the submitted value in descending order.
1. Parses a GET request that filters the articles by their tag value. e.g. `?tag=bootcamp`, `?tag=viral`

## Frontend Guidelines
1. Using **HTML5** tags and **SCSS**, the table should at the very least look visually as close as possible screen shot below.  Feel free to make any visual enhancements you deem necessary.
1. At the bottom of the table should be a Load More button (not shown below) that will make an xhr request to `more-articles.json` and dynamically adds them to the table.  Feel free to utilize JavaScript templating like mustache, handlebars, underscore, etc for rendering the rows.
1. Make use of the jQuery library [DataTables](http://datatables.net/download/build/jquery.dataTables.js) to enable sorting of rows by column type.
1. Add a button (not shown below) that transitions the header from a red gradient to a green gradient.  The transition should happen via CSS3 with a jQuery polyfill for older browsers.
1. The table should look the same in all modern browsers as well as Internet Explorer 7+.
1. The title and image should link to the articles.
1. Feel free to use any mixin or css authoring frameworks like compass, bourbon, etc.

[![End Product](https://bitbucket.org/policymic/dev-test/raw/master/screenshot.png)](https://bitbucket.org/policymic/dev-test/raw/master/screenshot.png)