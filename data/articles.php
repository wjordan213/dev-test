<?php
$articles = array(
    array(
        'id'        => 1,
        'title'     => 'North Koreans: Just "H\"angry',
        'url'       => 'http://www.policymic.com/articles/34441/war-with-north-korea-food-shortages-at-root-of-the-conflict',
        'firstname' => 'Chris',
        'lastname'  => 'Miles',
        'words'     => 400,
        'tag'       => 'Viral',
        'image'     => 'http://media1.policymic.com/site/articles/34441/1_photo-140x88.jpg',
        'submitted' => '2013-03-17 21:50:04'
    ),
    array(
        'id'        => 2,
        'title'     => '6 Ways to Make Your Articles Go Viral',
        'url'       => 'http://www.policymic.com/articles/34303/6-ways-to-make-your-articles-go-viral',
        'firstname' => 'Elizabeth',
        'lastname'  => 'Plank',
        'words'     => 501,
        'tag'       => 'Bootcamp',
        'image'     => 'http://media1.policymic.com/site/articles/34303/1_photo-140x88.jpg',
        'submitted' => '2013-02-18 21:50:04'
    ),
    array(
        'id'        => 3,
        'title'     => 'Uruguay Legalizes Same-Sex Marriage, Beats United States to the Punch',
        'url'       => 'http://www.policymic.com/articles/34423/gay-marriage-2013-uruguay-legalizes-same-sex-marriage-beats-united-states-to-the-punch',
        'firstname' => 'Michael',
        'lastname'  => 'McCutchen',
        'words'     => 502,
        'tag'       => 'Viral',
        'image'     => 'http://media1.policymic.com/site/articles/34423/1_photo-140x88.jpg',
        'submitted' => '2013-03-16 21:50:04'
    ),
    array(
        'id'        => 4,
        'title'     => 'Why Christopher Knight Abandoned Society For 27 Years',
        'url'       => 'http://www.policymic.com/articles/34101/maine-hermit-why-christopher-knight-abandoned-society-for-27-years',
        'firstname' => 'Michael',
        'lastname'  => 'Luciano',
        'words'     => 399,
        'tag'       => 'Bootcamp',
        'image'     => 'http://media1.policymic.com/site/articles/34101/1_photo-140x88.jpg',
        'submitted' => '2013-03-18 12:50:04'
    ),
    array(
        'id'        => 5,
        'title'     => 'Why is the U.S. More Violent Than Other Countries?',
        'url'       => 'http://www.policymic.com/articles/33985/gun-control-debate-2013-why-is-the-u-s-more-violent-than-other-countries',
        'firstname' => 'Jake',
        'lastname'  => 'Horowitz',
        'words'     => 401,
        'tag'       => 'Viral',
        'image'     => 'http://media1.policymic.com/site/articles/33985/2_photo-140x88.jpg',
        'submitted' => '2013-03-18 13:50:04'
    ),
    array(
        'id'        => 6,
        'title'     => 'Gun Control Legislation Actually Has a Good Chance in the Senate',
        'url'       => 'http://www.policymic.com/articles/34333/gun-control-vote-background-checks-legislation-actually-have-a-good-chance-in-the-senate',
        'firstname' => 'Chris',
        'lastname'  => 'Altchek',
        'words'     => 450,
        'tag'       => 'Bootcamp',
        'image'     => 'http://media1.policymic.com/site/articles/34333/1_photo-140x88.jpg',
        'submitted' => '2013-03-18 14:50:04'
    )
);